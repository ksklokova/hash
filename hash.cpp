#include <stdio.h>
#include <cmath>

#define BYTE unsigned char
#define DWORD unsigned long

using namespace std;

DWORD RotateLeft(DWORD val, int sh)
{	return (val<<sh|val>>(32-sh));	}

DWORD md5_F(DWORD x, DWORD y, DWORD z)
{	return ((x&y)|((~x)&z));	}
DWORD md5_G(DWORD x, DWORD y, DWORD z)
{	return ((x&z)|((~z)&y));	}
DWORD md5_H(DWORD x, DWORD y, DWORD z)
{	return (x^y^z);	}
DWORD md5_I(DWORD x, DWORD y, DWORD z)
{	return (y^((~z)|x));	}

DWORD sha1_F(DWORD m, DWORD l, DWORD k)
{	//return ((m&l)|((~m)&k));	}
	return (k^(m&(l^k)));	}
DWORD sha1_G(DWORD m, DWORD l, DWORD k)
{	return (m^l^k);	}
DWORD sha1_H(DWORD m, DWORD l, DWORD k)
{	//return ((m&l)|(m&k)|(l&k));	}
	return ((m&l)|(k&(m|l)));	}
// sha1_I = sha_G

DWORD ripemd160_F1(DWORD x, DWORD y, DWORD z)
{	return (x^y^z);	}
DWORD ripemd160_F2(DWORD x, DWORD y, DWORD z)
{	return ((x&y)|((~x)&z));	}
DWORD ripemd160_F3(DWORD x, DWORD y, DWORD z)
{	return ((x|(~y))^z);	}
DWORD ripemd160_F4(DWORD x, DWORD y, DWORD z)
{	return ((x&z)|(y&(~z)));	}
DWORD ripemd160_F5(DWORD x, DWORD y, DWORD z)
{	return (x^(y|(~z)));	}



int main(int argc, char * argv[])
//progname.exe [input_file] [output_file] [-m|-s|-r] 
{
	if (argc!=4) return -1;

	FILE *in=NULL, *out=NULL;
	if ((in=fopen(argv[1], "rb"))==NULL) return -2;
	if ((out=fopen(argv[2], "wb"))==NULL) return -3;
	
	if ((argv[3][0]!='-')||!(argv[3][1]=='m'||argv[3][1]=='s'||argv[3][1]=='r')) return -4;

	fseek(in, 0, SEEK_END);
	DWORD size=ftell(in), 
		Msize,		//razmer massiva M
		MDWsize;		//razmer massiva MDW
	if (size<0) return -5;
	if (size%64<56) Msize=(size/64+1)*64; //kolichestvo DWORDs: 512bites=16*32bites=16*4bytes=64bytes
	else Msize=(size/64+2)*64;
	BYTE *M=NULL;
	M = new BYTE[Msize];
	fseek(in, 0, SEEK_SET);
	long i, j;
	for (i=0; i<size; i++)
		fread(&M[i], sizeof(BYTE), 1, in);
	M[size]=0x80;
	for (i=size+1; i<Msize; i++)	M[i]=0;
	
	//__int64 
	long long size_bit=(DWORD)(size)*8;  
	
	DWORD *MDW;
	MDWsize=Msize/4;
	MDW = new DWORD[MDWsize];
	//if (argv[3][1]=='m') for(i=0; i<8; i++) 
	//	 M[Msize-8+i]=(size_bit >> i*8); 
	//if (argv[3][1]=='s') for(i=0; i<8; i++) 
	//	 M[Msize-1-i]=(size_bit >> i*8);
	//if (argv[3][1]=='r') for(i=0; i<8; i++) 
	//	 M[Msize-8+i]=(size_bit >> i*8); 
	////{	for(i=0; i<4; i++) M[Msize-5-i]=(size_bit >> i*8);
	////for(i=4; i<8; i++) M[Msize+3-i]=(size_bit >> i*8);	}


	//for (i=0; i<MDWsize; i++)
	//	MDW[i]=(DWORD)(M[i*4]) + (DWORD)(M[i*4+1]<<8) + (DWORD)(M[i*4+2]<<16) + (DWORD)(M[i*4+3]<<24);


	//=================================================       md5       =======================================================
	if (argv[3][1]=='m') 
	{
		for(i=0; i<8; i++) 
			M[Msize-8+i]=(size_bit >> i*8); 
		for (i=0; i<MDWsize; i++) //little-endian
			MDW[i]=(DWORD)(M[i*4]) + (DWORD)(M[i*4+1]<<8) + (DWORD)(M[i*4+2]<<16) + (DWORD)(M[i*4+3]<<24);
		
		//for (i=0; i<MDWsize; i++)
		//	fwrite(&MDW[i], sizeof(DWORD), 1, out);

		DWORD A=0x67452301,
			B=0xEFCDAB89,
			C=0x98BADCFE,
			D=0x10325476,
			T[65];
		for (i=1; i<=64; i++)
			T[i]=pow(2,32)*fabs(sin(i));

		//for (i=1; i<65; i++)
		//fwrite(&T[i], sizeof(DWORD), 1, out);

		DWORD AA, BB, CC, DD;

		for(i=0; i<MDWsize; i+=16)
		 {
			 AA = A; BB = B; CC = C; DD = D;
 
			 //raund 1
			 A = B + RotateLeft((A + md5_F(B,C,D) + MDW[i+ 0] + T[ 1]),  7);
			 D = A + RotateLeft((D + md5_F(A,B,C) + MDW[i+ 1] + T[ 2]), 12);
			 C = D + RotateLeft((C + md5_F(D,A,B) + MDW[i+ 2] + T[ 3]), 17);
			 B = C + RotateLeft((B + md5_F(C,D,A) + MDW[i+ 3] + T[ 4]), 22);
 
			 A = B + RotateLeft((A + md5_F(B,C,D) + MDW[i+ 4] + T[ 5]),  7);
			 D = A + RotateLeft((D + md5_F(A,B,C) + MDW[i+ 5] + T[ 6]), 12);
			 C = D + RotateLeft((C + md5_F(D,A,B) + MDW[i+ 6] + T[ 7]), 17);
			 B = C + RotateLeft((B + md5_F(C,D,A) + MDW[i+ 7] + T[ 8]), 22);
 
			 A = B + RotateLeft((A + md5_F(B,C,D) + MDW[i+ 8] + T[ 9]),  7);
			 D = A + RotateLeft((D + md5_F(A,B,C) + MDW[i+ 9] + T[10]), 12);
			 C = D + RotateLeft((C + md5_F(D,A,B) + MDW[i+10] + T[11]), 17);
			 B = C + RotateLeft((B + md5_F(C,D,A) + MDW[i+11] + T[12]), 22);
 
			 A = B + RotateLeft((A + md5_F(B,C,D) + MDW[i+12] + T[13]),  7);
			 D = A + RotateLeft((D + md5_F(A,B,C) + MDW[i+13] + T[14]), 12);
			 C = D + RotateLeft((C + md5_F(D,A,B) + MDW[i+14] + T[15]), 17);
			 B = C + RotateLeft((B + md5_F(C,D,A) + MDW[i+15] + T[16]), 22);
 
			 //raund 2
			 A = B + RotateLeft((A + md5_G(B,C,D) + MDW[i+ 1] + T[17]),  5);
			 D = A + RotateLeft((D + md5_G(A,B,C) + MDW[i+ 6] + T[18]),  9);
			 C = D + RotateLeft((C + md5_G(D,A,B) + MDW[i+11] + T[19]), 14);
			 B = C + RotateLeft((B + md5_G(C,D,A) + MDW[i+ 0] + T[20]), 20);
 
			 A = B + RotateLeft((A + md5_G(B,C,D) + MDW[i+ 5] + T[21]),  5);
			 D = A + RotateLeft((D + md5_G(A,B,C) + MDW[i+10] + T[22]),  9);
			 C = D + RotateLeft((C + md5_G(D,A,B) + MDW[i+15] + T[23]), 14);
			 B = C + RotateLeft((B + md5_G(C,D,A) + MDW[i+ 4] + T[24]), 20);
 
			 A = B + RotateLeft((A + md5_G(B,C,D) + MDW[i+ 9] + T[25]),  5);
			 D = A + RotateLeft((D + md5_G(A,B,C) + MDW[i+14] + T[26]),  9);
			 C = D + RotateLeft((C + md5_G(D,A,B) + MDW[i+ 3] + T[27]), 14);
			 B = C + RotateLeft((B + md5_G(C,D,A) + MDW[i+ 8] + T[28]), 20);
 
			 A = B + RotateLeft((A + md5_G(B,C,D) + MDW[i+13] + T[29]),  5);
			 D = A + RotateLeft((D + md5_G(A,B,C) + MDW[i+ 2] + T[30]),  9);
			 C = D + RotateLeft((C + md5_G(D,A,B) + MDW[i+ 7] + T[31]), 14);
			 B = C + RotateLeft((B + md5_G(C,D,A) + MDW[i+12] + T[32]), 20);
 
			 //raund 3
			 A = B + RotateLeft((A + md5_H(B,C,D) + MDW[i+ 5] + T[33]),  4);
			 D = A + RotateLeft((D + md5_H(A,B,C) + MDW[i+ 8] + T[34]), 11);
			 C = D + RotateLeft((C + md5_H(D,A,B) + MDW[i+11] + T[35]), 16);
			 B = C + RotateLeft((B + md5_H(C,D,A) + MDW[i+14] + T[36]), 23);
 
			 A = B + RotateLeft((A + md5_H(B,C,D) + MDW[i+ 1] + T[37]),  4);
			 D = A + RotateLeft((D + md5_H(A,B,C) + MDW[i+ 4] + T[38]), 11);
			 C = D + RotateLeft((C + md5_H(D,A,B) + MDW[i+ 7] + T[39]), 16);
			 B = C + RotateLeft((B + md5_H(C,D,A) + MDW[i+10] + T[40]), 23);
 
			 A = B + RotateLeft((A + md5_H(B,C,D) + MDW[i+13] + T[41]),  4);
			 D = A + RotateLeft((D + md5_H(A,B,C) + MDW[i+ 0] + T[42]), 11);
			 C = D + RotateLeft((C + md5_H(D,A,B) + MDW[i+ 3] + T[43]), 16);
			 B = C + RotateLeft((B + md5_H(C,D,A) + MDW[i+ 6] + T[44]), 23);
 
			 A = B + RotateLeft((A + md5_H(B,C,D) + MDW[i+ 9] + T[45]),  4);
			 D = A + RotateLeft((D + md5_H(A,B,C) + MDW[i+12] + T[46]), 11);
			 C = D + RotateLeft((C + md5_H(D,A,B) + MDW[i+15] + T[47]), 16);
			 B = C + RotateLeft((B + md5_H(C,D,A) + MDW[i+ 2] + T[48]), 23);
 
			 //raund 4
			 A = B + RotateLeft((A + md5_I(B,C,D) + MDW[i+ 0] + T[49]),  6);
			 D = A + RotateLeft((D + md5_I(A,B,C) + MDW[i+ 7] + T[50]), 10);
			 C = D + RotateLeft((C + md5_I(D,A,B) + MDW[i+14] + T[51]), 15);
			 B = C + RotateLeft((B + md5_I(C,D,A) + MDW[i+ 5] + T[52]), 21);
 
			 A = B + RotateLeft((A + md5_I(B,C,D) + MDW[i+12] + T[53]),  6);
			 D = A + RotateLeft((D + md5_I(A,B,C) + MDW[i+ 3] + T[54]), 10);
			 C = D + RotateLeft((C + md5_I(D,A,B) + MDW[i+10] + T[55]), 15);
			 B = C + RotateLeft((B + md5_I(C,D,A) + MDW[i+ 1] + T[56]), 21);
 
			 A = B + RotateLeft((A + md5_I(B,C,D) + MDW[i+ 8] + T[57]),  6);
			 D = A + RotateLeft((D + md5_I(A,B,C) + MDW[i+15] + T[58]), 10);
			 C = D + RotateLeft((C + md5_I(D,A,B) + MDW[i+ 6] + T[59]), 15);
			 B = C + RotateLeft((B + md5_I(C,D,A) + MDW[i+13] + T[60]), 21);
 
			 A = B + RotateLeft((A + md5_I(B,C,D) + MDW[i+ 4] + T[61]),  6);
			 D = A + RotateLeft((D + md5_I(A,B,C) + MDW[i+11] + T[62]), 10);
			 C = D + RotateLeft((C + md5_I(D,A,B) + MDW[i+ 2] + T[63]), 15);
			 B = C + RotateLeft((B + md5_I(C,D,A) + MDW[i+ 9] + T[64]), 21);
 
			 A += AA; B += BB; C += CC; D += DD;
		}

		fwrite(&A, sizeof(DWORD), 1, out);
		fwrite(&B, sizeof(DWORD), 1, out);
		fwrite(&C, sizeof(DWORD), 1, out);
		fwrite(&D, sizeof(DWORD), 1, out);

	}

	//=================================================       sha1       =======================================================
	if (argv[3][1]=='s') 
	{
		for(i=0; i<8; i++) 
			M[Msize-1-i]=(size_bit >> i*8);
		for (i=0; i<MDWsize; i++) //big-endian
			MDW[i]=(DWORD)(M[i*4]<<24) + (DWORD)(M[i*4+1]<<16) + (DWORD)(M[i*4+2]<<8) + (DWORD)(M[i*4+3]);

		//for (i=0; i<MDWsize; i++)
		//	fwrite(&MDW[i], sizeof(DWORD), 1, out);

		DWORD A = 0x67452301,
			B = 0xEFCDAB89,
			C = 0x98BADCFE,
			D = 0x10325476,
			E = 0xC3D2E1F0,
			a, b, c, d, e,
			W[80],
			k, f, temp;

		for(i=0; i<MDWsize; i+=16)
		{
			 for (j=0; j<16; j++)
				 W[j]=MDW[i+j];
			 for (j=16; j<80; j++)
				 W[j] = RotateLeft((W[j-3]^W[j-8]^W[j-14]^W[j-16]),1);
			 	
			 //for (j=0; j<80; j++)
				//fwrite(&W[j], sizeof(DWORD), 1, out);

			 a=A; b=B; c=C; d=D; e=E;

			 for (j=0; j<80; j++)
			 {
				 if (j<20)
				 {	 f=sha1_F(b,c,d);
					 k=0x5A827999;	 }
					//k=pow(2,30)*sqrt(2);	}
				 else if (j<40)
				 {	 f=sha1_G(b,c,d);
					 k=0x6ED9EBA1;	 }
					//k=pow(2,30)*sqrt(3);	}
				 else if (j<60)
				 {	 f=sha1_H(b,c,d);
					 k=0x8F1BBCDC;	 }
					//k=pow(2,30)*sqrt(5);	}
				 else
				 {	 f=sha1_G(b,c,d);
					 k=0xCA62C1D6;	 }
					//k=pow(2,30)*sqrt(10);	}


				 temp = RotateLeft(a,5) + f + k + W[j] + e;
				 e = d;
				 d = c;
				 c = RotateLeft(b,30);
				 b = a;
				 a = temp;
			 }

			 A+=a; B+=b; C+=c; D+=d; E+=e;
		}
		
		BYTE t;
		for(i=0; i<4; i++) 
		{	t=(A>>(3-i)*8)&0xff;
			fwrite(&t, sizeof(BYTE), 1, out);
		}
		for(i=0; i<4; i++) 
		{	t=(B>>(3-i)*8)&0xff;
			fwrite(&t, sizeof(BYTE), 1, out);
		}
		for(i=0; i<4; i++) 
		{	t=(C>>(3-i)*8)&0xff;
			fwrite(&t, sizeof(BYTE), 1, out);
		}
		for(i=0; i<4; i++) 
		{	t=(D>>(3-i)*8)&0xff;
			fwrite(&t, sizeof(BYTE), 1, out);
		}
		for(i=0; i<4; i++) 
		{	t=(E>>(3-i)*8)&0xff;
			fwrite(&t, sizeof(BYTE), 1, out);
		}


		//fwrite(&A, sizeof(DWORD), 1, out);
		//fwrite(&B, sizeof(DWORD), 1, out);
		//fwrite(&C, sizeof(DWORD), 1, out);
		//fwrite(&D, sizeof(DWORD), 1, out);
		//fwrite(&E, sizeof(DWORD), 1, out);
	}
	
	//=================================================       ripemd-160       =======================================================
	if (argv[3][1]=='r') 
	{
		for(i=0; i<8; i++) 
			M[Msize-8+i]=(size_bit >> i*8);
		for (i=0; i<MDWsize; i++) //little-endian
			MDW[i]=(DWORD)(M[i*4]) + (DWORD)(M[i*4+1]<<8) + (DWORD)(M[i*4+2]<<16) + (DWORD)(M[i*4+3]<<24);
				
		//for (i=0; i<MDWsize; i++)
		//	fwrite(&MDW[i], sizeof(DWORD), 1, out);

		DWORD //h0 = 0x01234567, 
			h0=0x67452301,
			//h1 = 0x89ABCDEF, 
			h1=0xEFCDAB89, 
			//h2 = 0xFEDCBA98, 
			h2=0x98BADCFE, 
			//h3 = 0x76543210, 
			h3=0x10325476, 
			//h4 = 0xF0E1D2C3, 
			h4=0xC3D2E1F0,
			A, B, C, D, E,
			AA, BB, CC, DD, EE,
			k, kk, f, ff, temp,
			r[80]={  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
					 7,  4, 13,  1, 10,  6, 15,  3, 12,  0,  9,  5,  2, 14, 11,  8, 
					 3, 10, 14,  4,  9, 15,  8,  1,  2,  7,  0,  6, 13, 11,  5, 12, 
					 1,  9, 11, 10,  0,  8, 12,  4, 13,  3,  7, 15, 14,  5,  6,  2,
					 4,  0,  5,  9,  7, 12,  2, 10, 14,  1,  3,  8, 11,  6, 15, 13},

			rr[80]={ 5, 14,  7,  0,  9,  2, 11,  4, 13,  6, 15,  8,  1, 10,  3, 12,
					 6, 11,  3,  7,  0, 13,  5, 10, 14, 15,  8, 12,  4,  9,  1,  2,
					15,  5,  1,  3,  7, 14,  6,  9, 11,  8, 12,  2, 10,  0,  4, 13,
					 8,  6,  4,  1,  3, 11, 15,  0,  5, 12,  2, 13,  9,  7, 10, 14,
					12, 15, 10,  4,  1,  5,  8,  7,  6,  2, 13, 14,  0,  3,  9, 11},

			s[80]={ 11,	14, 15, 12,  5,  8,  7,  9, 11, 13, 14, 15,  6,  7,  9,  8,
					 7,  6,  8, 13, 11,  9,  7, 15,  7, 12, 15,  9, 11,  7, 13, 12,
					11, 13,  6,  7, 14,  9, 13, 15, 14,  8, 13,  6,  5, 12,  7,  5,
					11, 12, 14, 15, 14, 15,  9,  8,  9, 14,  5,  6,  8,  6,  5, 12,
					 9, 15,  5, 11,  6,  8, 13, 12,  5, 12, 13, 14, 11,  8,  5,  6 },

			ss[80]={ 8,  9,  9, 11, 13, 15, 15,  5,  7,  7,  8, 11, 14, 14, 12,  6,
					 9, 13, 15,  7, 12,  8,  9, 11,  7,  7, 12,  7,  6, 15, 13, 11,
					 9,  7, 15, 11,  8,  6,  6, 14, 12, 13,  5, 14, 13, 13,  7,  5,
					15,  5,  8, 11, 14, 14,  6, 14,  6,  9, 12,  9, 12,  5, 15,  8,
					 8,  5, 12,  9, 12,  5, 14,  6,  8, 13,  6,  5, 15, 13, 11, 11 };
		
		for (i=0; i<MDWsize; i+=16)
		{
			A=AA=h0;
			B=BB=h1;
			C=CC=h2;
			D=DD=h3;
			E=EE=h4;

			for (j=0; j<80; j++)
			{
				if (j<16)
				{	k=0x00000000;	kk=0x50A28BE6;
					f=ripemd160_F1(B, C, D);
					ff=ripemd160_F5(BB, CC, DD);	}
				else if (j<32)
				{	k=0x5A827999;	kk=0x5C4DD124;
					f=ripemd160_F2(B, C, D);
					ff=ripemd160_F4(BB, CC, DD);	}
				else if (j<48)
				{	k=0x6ED9EBA1;	kk=0x6D703EF3;
					f=ripemd160_F3(B, C, D);	
					ff=ripemd160_F3(BB, CC, DD);	}
				else if (j<64)
				{	k=0x8F1BBCDC;	kk=0x7A6D76E9;
					f=ripemd160_F4(B, C, D);
					ff=ripemd160_F2(BB, CC, DD);	}
				else
				{	k=0xA953FD4E;	kk=0x00000000;
					f=ripemd160_F5(B, C, D);
					ff=ripemd160_F1(BB, CC, DD);	}

				temp= RotateLeft((A + f + MDW[i+ r[j]] + k),s[j]) + E;
				A = E; 
				E = D; 
				D = RotateLeft(C,10); 
				C = B; 
				B = temp;

				temp= RotateLeft((AA + ff + MDW[i+ rr[j]] + kk),ss[j]) + EE;
				AA = EE; 
				EE = DD; 
				DD = RotateLeft(CC,10); 
				CC = BB; 
				BB = temp;
			}

			temp = h1 + C + DD; 
			h1 = h2 + D + EE; 
			h2 = h3 + E + AA;
			h3 = h4 + A + BB; 
			h4 = h0 + B + CC; 
			h0 = temp;
		}
		
		fwrite(&h0, sizeof(DWORD), 1, out);
		fwrite(&h1, sizeof(DWORD), 1, out);
		fwrite(&h2, sizeof(DWORD), 1, out);
		fwrite(&h3, sizeof(DWORD), 1, out);
		fwrite(&h4, sizeof(DWORD), 1, out);
	}


	fclose(in);
	fclose(out);
	delete []M;
	delete []MDW;
	return 0;
}
